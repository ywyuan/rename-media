package com.oreo.rename.media.extractor;

import com.drew.imaging.ImageProcessingException;
import com.drew.metadata.Metadata;
import com.drew.metadata.exif.ExifDirectoryBase;
import com.drew.metadata.exif.ExifIFD0Directory;
import com.drew.metadata.exif.ExifSubIFDDirectory;
import com.drew.metadata.file.FileSystemDirectory;
import com.google.common.collect.Lists;
import com.oreo.rename.media.entity.enums.MediaTypeEnum;
import com.oreo.rename.media.util.DateTimeUtil;
import com.oreo.rename.media.util.MediaMetaUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

/**
 * JpegHeicExtractor
 *
 * @author yuan
 * @since 2023/9/25 17:24
 */
@Slf4j
@Component
public class ImageExtractor implements Extractor {

    /**
     * 降级使用文件修改时间
     * 不太准确，默认关闭
     */
    private static final boolean FALLBACK_FILE_MOD_TIME = false;

    @Override
    public List<MediaTypeEnum> supportExtList() {
        return Arrays.asList(MediaTypeEnum.IMG_HEIC,
                MediaTypeEnum.IMG_JPEG,
                MediaTypeEnum.IMG_PNG,
                MediaTypeEnum.IMG_JPG
        );
    }

    /**
     * 图片原始拍摄时间
     * jpeg heic
     *
     * @param file
     * @return
     * @throws ImageProcessingException
     * @throws IOException
     */
    @Override
    public LocalDateTime extractShotTime(File file) {

        String fileName = file.getName();
        Metadata metadata = MediaMetaUtil.extractMetadata(file);
        if (null == metadata) {
            return null;
        }

        ExifSubIFDDirectory directory = metadata.getFirstDirectoryOfType(ExifSubIFDDirectory.class);

        List<Date> priorityDateList = Lists.newArrayList();
        if (directory != null) {
            Date originalDate = directory.getDate(ExifDirectoryBase.TAG_DATETIME_ORIGINAL, TimeZone.getDefault());
            priorityDateList.add(originalDate);

            // 降级1
            Date digitizedDate = directory.getDate(ExifDirectoryBase.TAG_DATETIME_DIGITIZED, TimeZone.getDefault());
            priorityDateList.add(digitizedDate);
        }

        // 部分jpg
        ExifIFD0Directory ifd0 = metadata.getFirstDirectoryOfType(ExifIFD0Directory.class);
        if (null != ifd0) {
            Date ifd0Date = ifd0.getDate(ExifDirectoryBase.TAG_DATETIME, TimeZone.getDefault());
            priorityDateList.add(ifd0Date);
        }

        // 降级2
        if (FALLBACK_FILE_MOD_TIME) {
            FileSystemDirectory fileSystemDirectory = metadata.getFirstDirectoryOfType(FileSystemDirectory.class);
            Date fileModTime = fileSystemDirectory.getDate(FileSystemDirectory.TAG_FILE_MODIFIED_DATE, TimeZone.getDefault());
            priorityDateList.add(fileModTime);
        }

        Date[] array2Choose = priorityDateList.toArray(new Date[0]);
        Date shotDate = MediaMetaUtil.validDate(fileName, array2Choose);
        return DateTimeUtil.date2LocalDateTime(shotDate);
    }

}
