package com.oreo.rename.media.extractor;

import com.drew.metadata.Metadata;
import com.drew.metadata.MetadataException;
import com.drew.metadata.file.FileSystemDirectory;
import com.drew.metadata.mp4.Mp4Directory;
import com.drew.metadata.mp4.media.Mp4MediaDirectory;
import com.drew.metadata.mp4.media.Mp4VideoDirectory;
import com.oreo.rename.media.entity.domain.VideoMeta;
import com.oreo.rename.media.entity.enums.MediaTypeEnum;
import com.oreo.rename.media.util.ByteConverter;
import com.oreo.rename.media.util.DateTimeUtil;
import com.oreo.rename.media.util.MediaMetaUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

/**
 * Mp4Extractor
 *
 * @author yuan
 * @since 2023/9/25 17:27
 */
@Slf4j
@Component
public class Mp4Extractor extends VideoExtractor {
    @Override
    public List<MediaTypeEnum> supportExtList() {
        return Arrays.asList(MediaTypeEnum.V_MP4);
    }

    @Override
    public VideoMeta extractMeta(File file) {
        String fileName = file.getName();
        Metadata metadata = MediaMetaUtil.extractMetadata(file);
        if (null == metadata) {
            return null;
        }

        // 宽 高 帧率
        Mp4VideoDirectory videoDirectory = metadata.getFirstDirectoryOfType(Mp4VideoDirectory.class);
        if (null == videoDirectory) {
            LOGGER.error("File={} noVideoDirectory", fileName);
            return null;
        }
        Integer width = videoDirectory.getInteger(Mp4VideoDirectory.TAG_WIDTH);
        Integer height = videoDirectory.getInteger(Mp4VideoDirectory.TAG_HEIGHT);
        Integer frameRate = videoDirectory.getInteger(Mp4VideoDirectory.TAG_FRAME_RATE);

        // seconds
        Mp4Directory mp4Directory = metadata.getFirstDirectoryOfType(Mp4Directory.class);
        int durationSeconds = MediaMetaUtil.extractDurationSeconds(mp4Directory, Mp4Directory.TAG_DURATION_SECONDS);

        Double latitude = null;
        Double longitude = null;

        try {
            if (mp4Directory.containsTag(Mp4Directory.TAG_LATITUDE)) {
                latitude = mp4Directory.getDouble(Mp4Directory.TAG_LATITUDE);
            }
            if (mp4Directory.containsTag(Mp4Directory.TAG_LONGITUDE)) {
                longitude = mp4Directory.getDouble(Mp4Directory.TAG_LONGITUDE);
            }
        } catch (MetadataException e) {
            LOGGER.error("File={} no latitude or longitude", fileName, e);
        }

        // sizeM
        FileSystemDirectory fileSystemDirectory = metadata.getFirstDirectoryOfType(FileSystemDirectory.class);
        Long bytes = fileSystemDirectory.getLongObject(FileSystemDirectory.TAG_FILE_SIZE);
        long sizeM = ByteConverter.toMegabytes(bytes).longValue();

        // shotTime
        Date creationDate = videoDirectory.getDate(Mp4MediaDirectory.TAG_CREATION_TIME, TimeZone.getDefault());
        Date createDate = mp4Directory.getDate(Mp4Directory.TAG_CREATION_TIME);
        Date modTime = fileSystemDirectory.getDate(FileSystemDirectory.TAG_FILE_MODIFIED_DATE);

        Date shotDate = MediaMetaUtil.validDate(fileName, creationDate, createDate, modTime);

        return VideoMeta.builder()
                .name(fileName)
                .width(width)
                .height(height)
                .frameRate(frameRate)
                .durationSeconds(durationSeconds)
                .sizeM(sizeM)
                .shotDate(DateTimeUtil.date2LocalDateTime(shotDate))
                .latitude(latitude)
                .longitude(longitude)
                .build();
    }
}
