package com.oreo.rename.media.extractor;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.oreo.rename.media.entity.enums.MediaTypeEnum;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * ExtractorContext
 *
 * @author yuan
 * @since 2023/9/25 17:17
 */
@Component
public class ExtractorContext implements ApplicationContextAware {
    private static final Map<String, Extractor> EXTRACTOR_MAP = Maps.newHashMap();
    private static ApplicationContext appContext;

    public static List<String> supportExt() {
        initExtractor();

        List<String> extList = Lists.newArrayList();
        extList.addAll(EXTRACTOR_MAP.keySet());

        return extList;
    }

    public static VideoExtractor findVideoExtractor(String ext) {
        Extractor extractor = findExtractor(ext);
        if (extractor instanceof VideoExtractor videoExtractor) {
            return videoExtractor;
        } else {
            return null;
        }
    }

    public static Extractor findExtractor(String ext) {
        // registryExtractor
        initExtractor();

        return EXTRACTOR_MAP.get(ext.toLowerCase());
    }

    public static void registryExtractor(Extractor extractor) {
        List<MediaTypeEnum> mediaTypeEnums = extractor.supportExtList();
        for (MediaTypeEnum mediaTypeEnum : mediaTypeEnums) {
            EXTRACTOR_MAP.put(mediaTypeEnum.getExt().toLowerCase(), extractor);
        }
    }

    private static void initExtractor() {

        if (!EXTRACTOR_MAP.isEmpty()) {
            return;
        }

        List<Extractor> list = Lists.newArrayList();

        if (null == appContext) {
            list.add(new ImageExtractor());
            list.add(new MovExtractor());
            list.add(new Mp4Extractor());
        } else {
            Map<String, Extractor> beansOfType = appContext.getBeansOfType(Extractor.class);
            list.addAll(beansOfType.values());
        }

        // registry
        list.forEach(ExtractorContext::registryExtractor);
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        appContext = applicationContext;
    }
}
