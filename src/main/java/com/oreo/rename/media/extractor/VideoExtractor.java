package com.oreo.rename.media.extractor;

import com.oreo.rename.media.entity.domain.VideoMeta;

import java.io.File;
import java.time.LocalDateTime;

/**
 * VideoExtractor
 *
 * @author yuan
 * @since 2023/10/10 18:22
 */
public abstract class VideoExtractor implements Extractor {

    @Override
    public LocalDateTime extractShotTime(File file) {
        VideoMeta videoMeta = this.extractMeta(file);
        if (videoMeta == null) {
            return null;
        }

        return videoMeta.getShotDate();
    }

    /**
     * 提取视频meta信息
     *
     * @param file
     * @return
     */
    public abstract VideoMeta extractMeta(File file);

}
