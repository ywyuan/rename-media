package com.oreo.rename.media.extractor;

import com.drew.metadata.Metadata;
import com.drew.metadata.file.FileSystemDirectory;
import com.drew.metadata.mov.QuickTimeDirectory;
import com.drew.metadata.mov.media.QuickTimeMediaDirectory;
import com.drew.metadata.mov.media.QuickTimeVideoDirectory;
import com.drew.metadata.mov.metadata.QuickTimeMetadataDirectory;
import com.oreo.rename.media.entity.domain.VideoMeta;
import com.oreo.rename.media.entity.enums.MediaTypeEnum;
import com.oreo.rename.media.util.ByteConverter;
import com.oreo.rename.media.util.DateTimeUtil;
import com.oreo.rename.media.util.MediaMetaUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * MovExtractor
 *
 * @author yuan
 * @since 2023/9/25 17:26
 */
@Slf4j
@Component
public class MovExtractor extends VideoExtractor {
    @Override
    public List<MediaTypeEnum> supportExtList() {
        return Arrays.asList(MediaTypeEnum.V_MOV);
    }

    @Override
    public VideoMeta extractMeta(File file) {
        String fileName = file.getName();
        Metadata metadata = MediaMetaUtil.extractMetadata(file);
        if (null == metadata) {
            return null;
        }

        // 宽 高 帧率
        QuickTimeVideoDirectory videoDirectory = metadata.getFirstDirectoryOfType(QuickTimeVideoDirectory.class);

        if (null == videoDirectory) {
            LOGGER.error("File={} noVideoDirectory", fileName);
            return null;
        }

        Integer width = videoDirectory.getInteger(QuickTimeVideoDirectory.TAG_WIDTH);
        Integer height = videoDirectory.getInteger(QuickTimeVideoDirectory.TAG_HEIGHT);
        Integer frameRate = videoDirectory.getInteger(QuickTimeVideoDirectory.TAG_FRAME_RATE);

        // seconds
        QuickTimeDirectory directoryOfType = metadata.getFirstDirectoryOfType(QuickTimeDirectory.class);
        int durationSeconds = MediaMetaUtil.extractDurationSeconds(directoryOfType, QuickTimeDirectory.TAG_DURATION_SECONDS);

        // size M
        FileSystemDirectory fileSystemDirectory = metadata.getFirstDirectoryOfType(FileSystemDirectory.class);
        Long bytes = fileSystemDirectory.getLongObject(FileSystemDirectory.TAG_FILE_SIZE);
        long sizeM = ByteConverter.toMegabytes(bytes).longValue();

        // shotTime
        QuickTimeMetadataDirectory metadataDirectory = metadata.getFirstDirectoryOfType(QuickTimeMetadataDirectory.class);
        Date creationDate = null;
        if (null != metadataDirectory) {
            creationDate = metadataDirectory.getDate(QuickTimeMetadataDirectory.TAG_CREATION_DATE);
        }
        Date creationTime = videoDirectory.getDate(QuickTimeMediaDirectory.TAG_CREATION_TIME);
        Date modTime = fileSystemDirectory.getDate(FileSystemDirectory.TAG_FILE_MODIFIED_DATE);

        Date shotDate = MediaMetaUtil.validDate(fileName, creationDate, creationTime, modTime);

        return VideoMeta.builder()
                .name(fileName)
                .width(width)
                .height(height)
                .frameRate(frameRate)
                .durationSeconds(durationSeconds)
                .sizeM(sizeM)
                .shotDate(DateTimeUtil.date2LocalDateTime(shotDate))
                .build();
    }
}
