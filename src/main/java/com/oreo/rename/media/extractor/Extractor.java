package com.oreo.rename.media.extractor;

import com.oreo.rename.media.entity.enums.MediaTypeEnum;

import java.io.File;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Extractor
 *
 * @author yuan
 * @since 2023/9/25 17:17
 */

public interface Extractor {

    /**
     * 支持的文件类型
     *
     * @return
     */
    List<MediaTypeEnum> supportExtList();

    /**
     * 提取拍摄时间
     *
     * @param file
     * @return
     */
    LocalDateTime extractShotTime(File file);
}
