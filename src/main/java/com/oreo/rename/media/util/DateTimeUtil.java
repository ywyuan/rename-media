package com.oreo.rename.media.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * DateTimeUtil
 *
 * @author yuan
 * @since 2023/9/25 12:38
 */
public class DateTimeUtil {
    public static final Integer HUNDRED = 100;
    public static final Integer TEN_THOUSAND = 10_000;
    public static final LocalDateTime DEFAULT_DATE_TIME = LocalDateTime.of(1970, 1, 2, 8, 0, 0);
    public static final DateTimeFormatter DATE_FMT_1 = DateTimeFormatter.ofPattern("yyyyMM");
    public static final DateTimeFormatter DATE_FMT_2 = DateTimeFormatter.ofPattern("yyyy-MM");
    public static final DateTimeFormatter DATE_FMT_3 = DateTimeFormatter.ofPattern("yyyyMMdd");
    public static final DateTimeFormatter DATE_FMT_4 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    public static final DateTimeFormatter DATE_FMT_5 = DateTimeFormatter.ofPattern("yyyy/MM/dd");
    public static final DateTimeFormatter DATE_FMT_6 = DateTimeFormatter.ofPattern("yyyy年MM月dd日");
    public static final DateTimeFormatter DATE_FMT_7 = DateTimeFormatter.ofPattern("y-M-d");
    public static final DateTimeFormatter DATE_FMT_8 = DateTimeFormatter.ofPattern("yyyy.MM.dd");
    public static final DateTimeFormatter TIME_FMT_1 = DateTimeFormatter.ofPattern("HH:mm:ss");
    public static final DateTimeFormatter TIME_FMT_2 = DateTimeFormatter.ofPattern("HHmm");
    public static final DateTimeFormatter TIME_FMT_3 = DateTimeFormatter.ofPattern("HHmmss");
    public static final DateTimeFormatter TIME_FMT_4 = DateTimeFormatter.ofPattern("HH:mm");
    public static final DateTimeFormatter TIME_FMT_5 = DateTimeFormatter.ofPattern("H:m");
    public static final DateTimeFormatter DATE_TIME_FMT_1 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    public static final DateTimeFormatter DATE_TIME_FMT_2 = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
    public static final DateTimeFormatter DATE_TIME_FMT_3 = DateTimeFormatter.ofPattern("yyyy年MM月dd日 HH时mm分ss秒");
    public static final DateTimeFormatter DATE_TIME_FMT_4 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
    public static final DateTimeFormatter DATE_TIME_FMT_5 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
    public static final DateTimeFormatter DATE_TIME_FMT_6 = DateTimeFormatter.ofPattern("y-M-d H:m");
    private static final String WAVE = "~";

    private DateTimeUtil() {
    }

    public static long currentMills() {
        return System.currentTimeMillis();
    }

    public static long currentSeconds() {
        return TimeUnit.MILLISECONDS.toSeconds(currentMills());
    }

    /**
     * 获取date这天的开始时间
     *
     * @param date
     * @return
     */
    public static LocalDateTime beginOfDay(Date date) {
        if (null == date) {
            return null;
        }
        LocalDateTime ldt = date2LocalDateTime(date);
        return beginOfDay(ldt);
    }

    /**
     * 获取dateTime这天的开始时间
     *
     * @param dateTime
     * @return
     */
    public static LocalDateTime beginOfDay(LocalDateTime dateTime) {
        if (null == dateTime) {
            return null;
        }
        return beginOfDay(dateTime.toLocalDate());
    }

    /**
     * 获取ld这天的开始时间
     *
     * @param ld
     * @return
     */
    public static LocalDateTime beginOfDay(LocalDate ld) {
        if (null == ld) {
            return null;
        }
        return LocalDateTime.of(ld, LocalTime.MIN);
    }

    /**
     * 获取date这天的结束时间
     *
     * @param date
     * @return
     */
    public static LocalDateTime endOfDay(Date date) {
        if (null == date) {
            return null;
        }
        LocalDateTime ldt = date2LocalDateTime(date);

        return endOfDay(ldt);
    }

    /**
     * 获取dateTime这天的结束时间
     *
     * @param dateTime
     * @return
     */
    public static LocalDateTime endOfDay(LocalDateTime dateTime) {
        if (null == dateTime) {
            return null;
        }
        return endOfDay(dateTime.toLocalDate());
    }

    /**
     * 获取ld这天的结束时间
     *
     * @param ld
     * @return
     */
    public static LocalDateTime endOfDay(LocalDate ld) {
        if (null == ld) {
            return null;
        }
        return LocalDateTime.of(ld, LocalTime.MAX);
    }


    /**
     * 校验日期时间是否是指定格式
     *
     * @param text    传入时间
     * @param pattern 日期格式
     * @return
     */
    public static boolean checkDateTime(String text, String pattern) {
        DateFormat formatter = new SimpleDateFormat(pattern);
        try {
            Date date = formatter.parse(text);
            return text.equals(formatter.format(date));
        } catch (ParseException e) {
            return false;
        }
    }

    /**
     * 校验日期是否是指定格式
     *
     * @param text
     * @param formatter
     * @return
     */
    public static boolean checkDate(String text, DateTimeFormatter formatter) {
        try {
            parseDate(formatter, text);
        } catch (DateTimeParseException e) {
            return false;
        }

        return true;
    }

    /**
     * 校验时间是否是指定格式
     *
     * @param text
     * @param formatter
     * @return
     */
    public static boolean checkTime(String text, DateTimeFormatter formatter) {
        try {
            parseTime(formatter, text);
        } catch (DateTimeParseException e) {
            return false;
        }

        return true;
    }

    /**
     * 校验日期时间是否是指定格式
     *
     * @param text
     * @param formatter
     * @return
     */
    public static boolean checkDateTime(String text, DateTimeFormatter formatter) {
        try {
            parse(formatter, text);
        } catch (DateTimeParseException e) {
            return false;
        }

        return true;
    }

    /**
     * 解析日期时间
     *
     * @param formatter
     * @param s
     * @return
     */
    public static LocalDateTime parse(DateTimeFormatter formatter, String s) {
        return LocalDateTime.parse(s, formatter);
    }

    /**
     * 解析日期
     *
     * @param formatter
     * @param s
     * @return
     */
    public static LocalDate parseDate(DateTimeFormatter formatter, String s) {
        return LocalDate.parse(s, formatter);
    }

    /**
     * 解析时间
     *
     * @param formatter
     * @param s
     * @return
     */
    public static LocalTime parseTime(DateTimeFormatter formatter, String s) {
        return LocalTime.parse(s, formatter);
    }

    public static LocalDateTime parse(String s) {
        return parse(DATE_TIME_FMT_1, s);
    }

    public static String format(DateTimeFormatter formatter, LocalDateTime localDateTime) {
        return formatter.format(localDateTime);
    }

    public static String format(DateTimeFormatter formatter, LocalDate localDate) {
        return formatter.format(localDate);
    }

    public static String format(LocalDateTime localDateTime) {
        return format(DATE_TIME_FMT_1, localDateTime);
    }


    /**
     * format as YYYYMMDD
     *
     * @param date
     * @return
     */
    public static Integer formatYmd(Date date) {
        if (null == date) {
            return null;
        }
        LocalDateTime ldt = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        return formatYmd(ldt);
    }

    /**
     * format as YYYYMMDD
     *
     * @param ld
     * @return
     */
    public static Integer formatYmd(LocalDate ld) {
        if (null == ld) {
            return null;
        }
        String format = DATE_FMT_3.format(ld);

        return Integer.valueOf(format);
    }

    /**
     * format as YYYYMMDD
     *
     * @param ldt
     * @return
     */
    public static Integer formatYmd(LocalDateTime ldt) {
        if (null == ldt) {
            return null;
        }
        return formatYmd(ldt.toLocalDate());
    }

    /**
     * format as 115  for 1:15
     *
     * @param date
     * @return
     */
    public static Integer formatHm(Date date) {
        if (null == date) {
            return null;
        }
        LocalDateTime ldt = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        return formatHm(ldt);
    }

    /**
     * format as 115  for 1:15:00
     *
     * @param ldt
     * @return
     */
    public static Integer formatHm(LocalDateTime ldt) {
        if (null == ldt) {
            return null;
        }
        String format = TIME_FMT_2.format(ldt);
        return Integer.valueOf(format);
    }

    /**
     * format as "01:15"  for 1:15:00
     *
     * @param lt
     * @return
     */
    public static String formatHm(LocalTime lt) {
        if (null == lt) {
            return "";
        }
        return TIME_FMT_4.format(lt);
    }

    /**
     * format as "01:15~02:25"  for 1:15:00,2:25:00
     *
     * @param lt1
     * @param lt2
     * @return
     */
    public static String formatHmRange(LocalTime lt1, LocalTime lt2) {
        return formatHm(lt1) + WAVE + formatHm(lt2);
    }

    /**
     * format as 11508  for 1:15:08
     *
     * @param date
     * @return
     */
    public static Integer formatHms(Date date) {
        if (null == date) {
            return null;
        }
        LocalDateTime ldt = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        return formatHms(ldt);
    }

    /**
     * format as 11508  for 1:15:08
     *
     * @param ldt
     * @return
     */
    public static Integer formatHms(LocalDateTime ldt) {
        if (null == ldt) {
            return null;
        }
        String format = TIME_FMT_3.format(ldt);
        return Integer.valueOf(format);
    }

    /**
     * format as "01:15:08"  for 1:15:08
     *
     * @param lt
     * @return
     */
    public static String formatHms(LocalTime lt) {
        if (null == lt) {
            return null;
        }
        return TIME_FMT_1.format(lt);
    }

    /**
     * 计算 daysAfter 天后的年月日
     *
     * @param curYmd
     * @param daysAfter
     * @return
     */
    public static Integer afterDays(Integer curYmd, int daysAfter) {
        int year = curYmd / TEN_THOUSAND;
        int day = curYmd % HUNDRED;
        int mon = curYmd / HUNDRED % HUNDRED;
        LocalDate nextLocalDate = LocalDate.of(year, mon, day).plusDays(daysAfter);
        return formatYmd(nextLocalDate);
    }

    public static LocalDateTime seconds2LocalDateTime(Long seconds) {
        if (null == seconds) {
            return null;
        }
        long millis = TimeUnit.SECONDS.toMillis(seconds);
        return LocalDateTime.ofInstant(Instant.ofEpochMilli(millis), ZoneId.systemDefault());
    }

    public static LocalDateTime mill2LocalDateTime(Long milliSeconds) {
        if (null == milliSeconds) {
            return null;
        }
        return LocalDateTime.ofInstant(Instant.ofEpochMilli(milliSeconds), ZoneId.systemDefault());
    }

    public static LocalDateTime date2LocalDateTime(Date date) {
        if (null == date) {
            return null;
        }
        return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
    }


    public static Long convert2Second(LocalDateTime localDateTime) {
        if (localDateTime == null) {
            return null;
        }

        Long milliSecond = convert2Mills(localDateTime);
        return TimeUnit.MILLISECONDS.toSeconds(milliSecond);
    }

    public static Long convert2Mills(LocalDateTime localDateTime) {
        if (localDateTime == null) {
            return null;
        }
        return localDateTime.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
    }

    public static Date convert2Date(LocalDateTime localDateTime) {
        if (localDateTime == null) {
            return null;
        }
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }

}
