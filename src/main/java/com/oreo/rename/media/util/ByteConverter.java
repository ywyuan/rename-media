package com.oreo.rename.media.util;

/**
 * ByteConverter
 *
 * @author yuan
 * @since 2023/10/5 17:49
 */
public class ByteConverter {
    /**
     * 1 Byte = 8 bits
     * 1 Kilobyte (K) = 1024 Bytes
     * 1 Megabyte (M) = 1024 Kilobytes
     * 1 Gigabyte (G) = 1024 Megabytes
     * 1 Terabyte (T) = 1024 Gigabytes
     * 1 Petabyte (P) = 1024 Terabytes
     * 1 Exabyte (E) = 1024 Petabytes
     */
    public static final int BYTES_PER_KILOBYTE = 1024;
    public static final int BYTES_PER_MEGABYTE = BYTES_PER_KILOBYTE * BYTES_PER_KILOBYTE;
    public static final int BYTES_PER_GIGABYTE = BYTES_PER_MEGABYTE * BYTES_PER_KILOBYTE;
    public static final long BYTES_PER_TERABYTE = BYTES_PER_GIGABYTE * BYTES_PER_KILOBYTE;
    public static final long BYTES_PER_PETABYTE = BYTES_PER_TERABYTE * BYTES_PER_KILOBYTE;
    public static final long BYTES_PER_EXABYTE = BYTES_PER_PETABYTE * BYTES_PER_KILOBYTE;


    public static Double toKilobytes(long bytes) {
        return (double) bytes / BYTES_PER_KILOBYTE;
    }

    public static Double toMegabytes(long bytes) {
        return (double) bytes / BYTES_PER_MEGABYTE;
    }

    public static Double toGigabytes(long bytes) {
        return (double) bytes / BYTES_PER_GIGABYTE;
    }

    public static Double toTerabytes(long bytes) {
        return (double) bytes / BYTES_PER_TERABYTE;
    }

    public static Double toPetabytes(long bytes) {
        return (double) bytes / BYTES_PER_PETABYTE;
    }

    public static Double toExabytes(long bytes) {
        return (double) bytes / BYTES_PER_EXABYTE;
    }
}
