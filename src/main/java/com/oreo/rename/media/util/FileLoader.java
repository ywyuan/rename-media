package com.oreo.rename.media.util;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

import java.io.File;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * FileLoader
 *
 * @author yuan
 * @since 2023/10/10 16:07
 */
@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class FileLoader {

    public static List<File> load(String path) {
        return load(path, false, Collections.emptyList());
    }

    public static List<File> load(String path, boolean recursively) {
        return load(path, recursively, Collections.emptyList());
    }

    public static List<File> load(String path, boolean recursively, List<String> filterExtList) {
        Set<String> lowerExt = Sets.newHashSet();
        if (null != filterExtList) {
            lowerExt = filterExtList.stream().map(String::toLowerCase).collect(Collectors.toSet());
        }

        final Set<String> finalLowerExt = lowerExt;
        return load(path, recursively, ((name, ext) -> finalLowerExt.isEmpty() || finalLowerExt.contains(ext.toLowerCase())));
    }

    public static List<File> load(String path, boolean recursively, FileLoadFilter filter) {
        File folder = new File(path);
        File[] files = folder.listFiles();
        List<File> fileList = Lists.newArrayList();
        if (null == files) {
            LOGGER.warn("Found 0 files under invalid path=[{}]", path);
            return fileList;
        }
        if (files.length == 0) {
            return fileList;
        }

        for (File file : files) {
            String name = file.getName();
            // 跳过隐藏文件
            if (name.startsWith(".")) {
                continue;
            }

            boolean directory = file.isDirectory();
            // 递归获取文件夹下的所有子文件
            if (directory && recursively) {
                List<File> subFiles = load(file.getAbsolutePath(), recursively, filter);
                fileList.addAll(subFiles);
                continue;
            }

            // 不递归的 跳过文件夹 . .. .DS_Store
            if (directory) {
                continue;
            }

            String ext = findFileExt(name);
            if (!StringUtils.hasText(ext)) {
                LOGGER.warn("no ext for file {} under {}", name, file.getAbsolutePath());
                continue;
            }

            int nameNoExtLen = name.length() - ext.length() - 1;
            String nameNoExt = name.substring(0, nameNoExtLen);
            if (filter.accept(nameNoExt, ext)) {
                fileList.add(file);
            }
        }

        int size = fileList.size();
        if (size > 0) {
            LOGGER.info("Found {} files under path=[{}]", size, path);
        }

        return fileList;
    }

    public static String findFileExt(String fileName) {
        int i = fileName.lastIndexOf(".");
        if (i < 0) {
            return null;
        }
        String dotExt = fileName.substring(i);

        if (dotExt.length() <= 1) {
            return null;
        }
        // 去除点号
        return dotExt.substring(1).toLowerCase();
    }
}
