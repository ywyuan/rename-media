package com.oreo.rename.media.util;

import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.MetadataException;
import com.oreo.rename.media.processor.MediaProcessor;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

/**
 * MetaUtil
 *
 * @author yuan
 * @since 2023/10/12 16:17
 */
@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class MediaMetaUtil {

    public static final LocalDateTime VALID_MIN_DATE = LocalDateTime.of(1990, 1, 1, 0, 0, 0);


    /**
     * 默认提取metadata的方法
     *
     * @param file
     * @return
     */
    public static Metadata extractMetadata(File file) {
        Metadata metadata;
        try {
            metadata = ImageMetadataReader.readMetadata(file);
            return metadata;
        } catch (ImageProcessingException | IOException e) {
            LOGGER.error("File={} readMetadata failed msg={}", file.getName(), e.getMessage(), e);
            return null;
        }
    }

    /**
     * 判断日期是否有效
     *
     * @param fileName
     * @param dates
     * @return
     */
    public static Date validDate(String fileName, Date... dates) {
        if (dates == null || dates.length == 0) {
            return null;
        }

        for (int i = 0; i < dates.length; i++) {
            Date date = dates[i];
            if (null == date) {
                continue;
            }
            LocalDateTime localDateTime = DateTimeUtil.date2LocalDateTime(date);
            if (localDateTime.isAfter(VALID_MIN_DATE)) {
                if (i != 0) {
                    recordFallBack(fileName, date, i);
                }
                return date;
            }
        }

        // not match back to first
        Date backDate = dates[0];

        recordFallBack(fileName, backDate, 0);

        return backDate;
    }

    private static void recordFallBack(String fileName, Date date, int i) {
        if (null == date) {
            return;
        }

        LocalDateTime localDateTime = DateTimeUtil.date2LocalDateTime(date);
        String formatStr = localDateTime.format(MediaProcessor.formatter);
        String msg = String.format("date=%s of file=%s index=%s", formatStr, fileName, i);
        LOGGER.warn("mind choose {}", msg);

        List<String> list = MediaProcessor.fallBackShotTimeFiles.get();
        list.add(msg);
    }

    public static int extractDurationSeconds(Directory directory, int tagKey) {
        try {
            float aFloat = directory.getFloat(tagKey);
            // 四舍五入
            return Math.round(aFloat);
        } catch (MetadataException e) {
            return directory.getInteger(tagKey);
        }
    }
}
