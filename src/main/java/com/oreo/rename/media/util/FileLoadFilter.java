package com.oreo.rename.media.util;

/**
 * FileLoadFilter
 *
 * @author yuan
 * @since 2023/10/10 16:16
 */
@FunctionalInterface
public interface FileLoadFilter {

    /**
     * Tests whether or not the specified abstract pathname should be
     * included in a pathname list.
     *
     * @param name The name of the file to be tested
     * @param ext  The extension of the file to be tested
     * @return {@code true} if and only if all matched.
     */
    boolean accept(String name, String ext);
}
