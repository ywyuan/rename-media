package com.oreo.rename.media.service;

import com.oreo.rename.media.entity.domain.VideoMeta;

import java.util.List;
import java.util.Map;

/**
 * MediaRenameService
 *
 * @author yuan
 * @since 2023/9/25 14:20
 */
public interface MediaService {

    /**
     * support
     *
     * @return
     */
    List<String> support();

    /**
     * check
     * 检查目录下文件情况：
     * 数量：总-可识别-不可识别-格式-数量
     * 媒体：小时分布
     * 命名冲突：个数（源文件-冲突文件-转移情况）
     *
     * @param path
     * @param recursively
     * @param filterExtList
     * @return
     */
    Map<String, List<String>> check(String path, boolean recursively, List<String> filterExtList);

    /**
     * rename
     *
     * @param path
     * @param recursively
     * @param filterExtList
     * @return
     */
    Map<String, List<String>> rename(String path, Integer wrongYearIfLess, boolean recursively, List<String> filterExtList);


    /**
     * 获取视频 meta 信息
     *
     * @param path
     * @param recursively
     * @param filterExtList
     * @return
     */
    List<VideoMeta> videoMeta(String path, boolean recursively, List<String> filterExtList);

    /**
     * 视频是否可压缩
     *
     * @param path
     * @param recursively
     * @param filterExtList
     * @return
     */
    List<VideoMeta> videoCanCompress(String path, boolean recursively, List<String> filterExtList);

}
