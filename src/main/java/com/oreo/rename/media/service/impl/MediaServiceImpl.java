package com.oreo.rename.media.service.impl;

import com.google.common.collect.Lists;
import com.oreo.rename.media.entity.domain.VideoMeta;
import com.oreo.rename.media.extractor.ExtractorContext;
import com.oreo.rename.media.processor.MediaProcessor;
import com.oreo.rename.media.service.MediaService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.Map;

/**
 * MediaRenameServiceImpl
 *
 * @author yuan
 * @since 2023/9/25 14:20
 */
@Slf4j
@Service
public class MediaServiceImpl implements MediaService {

    /**
     * 1080P 30fps 足够,超过则认为可压缩
     *
     * @param videoMeta
     * @return
     */
    private static boolean canCompress(VideoMeta videoMeta) {
        Integer width = videoMeta.getWidth();
        Integer height = videoMeta.getHeight();
        return width * height > 1920 * 1080;

//        Integer frameRate = videoMeta.getFrameRate();
//        return frameRate > 30;
    }

    @Override
    public List<String> support() {
        return ExtractorContext.supportExt();
    }

    @Override
    public Map<String, List<String>> check(String path, boolean recursively, List<String> filterExtList) {
        MediaProcessor processor = new MediaProcessor(path)
                .setRecursively(recursively)
                .setFilterExtList(filterExtList);

        return processor.process();
    }

    @Override
    public Map<String, List<String>> rename(String path, Integer wrongYearIfLess, boolean recursively, List<String> filterExtList) {
        MediaProcessor processor = new MediaProcessor(path)
                .setWrongYearIfLess(wrongYearIfLess)
                .setRecursively(recursively)
                .setFilterExtList(filterExtList);

        return processor.process(true);
    }

    @Override
    public List<VideoMeta> videoMeta(String path, boolean recursively, List<String> filterExtList) {

        MediaProcessor processor = new MediaProcessor(path)
                .setRecursively(recursively)
                .setFilterExtList(filterExtList);
        List<VideoMeta> metaList = processor.getVideoMetaList();
        metaList.sort(Comparator.comparing(VideoMeta::getSizeM).reversed());
        return metaList;
    }

    @Override
    public List<VideoMeta> videoCanCompress(String path, boolean recursively, List<String> filterExtList) {
        List<VideoMeta> metaList = this.videoMeta(path, recursively, filterExtList);

        List<VideoMeta> canCompressVideo = Lists.newArrayList();
        for (VideoMeta videoMeta : metaList) {
            if (canCompress(videoMeta)) {
                canCompressVideo.add(videoMeta);
            }
        }

        return canCompressVideo;
    }
}
