package com.oreo.rename.media.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * Result
 *
 * @author yuan
 * @since 2023/9/25 14:11
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Result<T> implements Serializable {

    private int code;
    private String msg;
    private T data;

    public static <T> Result<T> error(int code, String msg) {
        return new Result(code, msg, null);
    }


    public static <T> Result<T> success() {
        return new Result(0, "OK", null);
    }

    public static <T> Result<T> success(T data) {
        return new Result(0, "OK", data);
    }
}
