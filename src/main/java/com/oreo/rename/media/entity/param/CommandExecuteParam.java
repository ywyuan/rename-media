package com.oreo.rename.media.entity.param;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * CommandExecuteParam
 *
 * @author yuan
 * @since 2023/10/11 11:49
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommandExecuteParam {


    private List<String> cmdList;
}
