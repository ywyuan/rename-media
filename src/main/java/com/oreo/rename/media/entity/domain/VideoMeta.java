package com.oreo.rename.media.entity.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * VideoMeta
 *
 * @author yuan
 * @since 2023/10/4 20:42
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class VideoMeta {
    private String name;
    private long sizeM;
    private Integer frameRate;
    private Integer width;
    private Integer height;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime shotDate;
    private long durationSeconds;

    /**
     * 维度
     */
    private Double latitude;
    /**
     * 经度
     */
    private Double longitude;
}
