package com.oreo.rename.media.entity.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import static com.oreo.rename.media.common.Const.IMG;
import static com.oreo.rename.media.common.Const.VIDEO;

/**
 * MediaTypeEnum
 *
 * @author yuan
 * @since 2023/9/25 14:35
 */
@Getter
@NoArgsConstructor
@AllArgsConstructor
public enum MediaTypeEnum {

    // 图像
    IMG_JPEG(IMG, "JPEG"),
    IMG_HEIC(IMG, "HEIC"),
    IMG_JPG(IMG, "JPG"),
    IMG_PNG(IMG, "PNG"),
    IMG_BMP(IMG, "BMP"),
    IMG_GIF(IMG, "GIF"),
    IMG_TIFF(IMG, "TIFF"),
    IMG_SVG(IMG, "SVG"),

    // 视频
    V_MOV(VIDEO, "MOV"),
    V_MP4(VIDEO, "MP4"),
    V_AVI(VIDEO, "AVI"),
    V_MPG(VIDEO, "MPG"),
    V_MPEG(VIDEO, "MPEG"),
    V_WMV(VIDEO, "WMV"),
    V_MKV(VIDEO, "MKV"),
    V_M4V(VIDEO, "M4V"),
    V_RMVB(VIDEO, "RMVB"),
    V_MTV(VIDEO, "MTV"),
    V_3GP(VIDEO, "3GP"),
    ;
    private String type;
    private String ext;


    public static boolean support(String fileExt) {

        for (MediaTypeEnum value : MediaTypeEnum.values()) {
            if (value.getExt().equalsIgnoreCase(fileExt)) {
                return true;
            }
        }

        return false;
    }
}
