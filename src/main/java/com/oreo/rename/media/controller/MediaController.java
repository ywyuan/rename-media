package com.oreo.rename.media.controller;

import com.google.common.collect.Lists;
import com.oreo.rename.media.entity.Result;
import com.oreo.rename.media.service.MediaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

/**
 * RenameController
 *
 * @author yuan
 * @since 2023/9/25 14:10
 */
@RequestMapping("/media/")
@RestController
public class MediaController {

    @Autowired
    private MediaService mediaService;


    /**
     * 支持的多媒体文件类型
     * 127.0.0.1:8080/media/support
     *
     * @return
     */
    @GetMapping("support")
    public Result support() {
        return Result.success(mediaService.support());
    }

    /**
     * 检查目录下文件情况：
     * 数量：总-可识别-不可识别
     * 图片：格式+数量+小时分布
     * 视频：格式+数量+小时分布
     * 命名冲突：个数（源文件-冲突文件-转移情况）
     * 127.0.0.1:8080/media/check?path=/Users/yuan/Downloads/2023xx
     *
     * @return
     */
    @GetMapping("check")
    public Result<Object> check(String path,
                                @RequestParam(required = false, defaultValue = "false") Boolean recursively,
                                @RequestParam(required = false, defaultValue = "") String filterExtStr) {

        List<String> filterExtList = Lists.newArrayList();
        if (StringUtils.hasText(filterExtStr)) {
            filterExtList = Arrays.asList(filterExtStr.split(","));
        }

        return Result.success(mediaService.check(path, recursively, filterExtList));
    }


    /**
     * 执行重命名
     * 127.0.0.1:8080/media/rename?path=/Users/yuan/Downloads/2023xx
     *
     * @return
     */
    @GetMapping("rename")
    public Result rename(@RequestParam String path,
                         @RequestParam(required = false, defaultValue = "2000") Integer wrongYearIfLess,
                         @RequestParam(required = false, defaultValue = "false") Boolean recursively,
                         @RequestParam(required = false, defaultValue = "") String filterExtStr) {

        List<String> filterExtList = Lists.newArrayList();
        if (StringUtils.hasText(filterExtStr)) {
            filterExtList = Arrays.asList(filterExtStr.split(","));
        }
        return Result.success(mediaService.rename(path, wrongYearIfLess, recursively, filterExtList));
    }

    /**
     * 获取视频 meta 信息
     * 127.0.0.1:8080/media/video/meta?path=/Users/yuan/Downloads/2023xxvideo
     *
     * @param path         扫描文件路径
     * @param recursively  是否递归获取子文件夹下的文件
     * @param filterExtStr 文件类型筛选, eg: mov,mp4
     * @return
     */
    @GetMapping("video/meta")
    public Result videoMeta(@RequestParam String path,
                            @RequestParam(required = false, defaultValue = "false") Boolean recursively,
                            @RequestParam(required = false, defaultValue = "") String filterExtStr) {
        List<String> filterExtList = Lists.newArrayList();
        if (StringUtils.hasText(filterExtStr)) {
            filterExtList = Arrays.asList(filterExtStr.split(","));
        }

        return Result.success(mediaService.videoMeta(path, recursively, filterExtList));
    }

    /**
     * 视频是否可压缩
     * 127.0.0.1:8080/media/video/can/compress?path=/Users/yuan/Downloads/2023xxvideo
     *
     * @param path
     * @param recursively
     * @param filterExtStr
     * @return
     */
    @GetMapping("video/can/compress")
    public Result videoCanCompress(@RequestParam String path,
                                   @RequestParam(required = false, defaultValue = "false") Boolean recursively,
                                   @RequestParam(required = false, defaultValue = "") String filterExtStr) {
        List<String> filterExtList = Lists.newArrayList();
        if (StringUtils.hasText(filterExtStr)) {
            filterExtList = Arrays.asList(filterExtStr.split(","));
        }
        return Result.success(mediaService.videoCanCompress(path, recursively, filterExtList));
    }
}
