package com.oreo.rename.media.controller;

import com.oreo.rename.media.entity.Result;
import com.oreo.rename.media.entity.param.CommandExecuteParam;
import com.oreo.rename.media.processor.CommandExecutor;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

/**
 * CommandController
 *
 * @author yuan
 * @since 2023/10/11 11:37
 */
@RequestMapping("/command/")
@RestController
public class CommandController {

//    "pwd",
//            "cd ~/Downloads/2023xxvideo",
//            "ls -lSh",
//            "date",
//            "ffmpeg -version"

    /**
     * 批量执行shell命令
     * 127.0.0.1:8080/command/batch/execute
     * <p>
     * curl --location --request POST '127.0.0.1:8080/command/batch/execute' \
     * --header 'Content-Type: application/json' \
     * --data-raw '{
     * "cmdList": [
     * "pwd",
     * "cd ~/Downloads/2023xxvideo",
     * "ls -lSh |head -5 ",
     * "pwd"
     * ]
     * }'
     * <p>
     *
     * @param param
     * @return
     */
    @PostMapping("batch/execute")
    public Result batchExecute(@RequestBody CommandExecuteParam param) {
        List<String> cmdList = param.getCmdList();
        if (CollectionUtils.isEmpty(cmdList)) {
            return Result.error(400, "cmdList 为空");
        }

        String cmsStr = cmdList.stream().collect(Collectors.joining("&&"));
        String commandRes = CommandExecutor.execCommand(true, cmsStr);
        return Result.success(commandRes);
    }
}
