package com.oreo.rename.media;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RenameMediaApplication {

    public static void main(String[] args) {
        SpringApplication.run(RenameMediaApplication.class, args);
    }

}
