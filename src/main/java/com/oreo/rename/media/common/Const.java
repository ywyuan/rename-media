package com.oreo.rename.media.common;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * Const
 *
 * @author yuan
 * @since 2023/9/25 14:44
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Const {
    /**
     * 图像
     */
    public static final String IMG = "IMAGE";

    /**
     * 视频
     */
    public static final String VIDEO = "VIDEO";
}
