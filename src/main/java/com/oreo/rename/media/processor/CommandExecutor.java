package com.oreo.rename.media.processor;

import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;

/**
 * CommandExecutor
 *
 * @author yuan
 * @since 2023/10/4 18:32
 */
@Slf4j
public class CommandExecutor {

    /**
     * 每条命令独立执行
     *
     * @param printResult
     * @param cmdList
     * @return
     */
    public static Map<String, String> execCommandsIndividual(Boolean printResult, String... cmdList) {
        Map<String, String> cmdRes = Maps.newLinkedHashMap();
        for (String cmd : cmdList) {
            String res = execCommand(printResult, cmd);
            cmdRes.put(cmd, res);
        }

        return cmdRes;
    }

    /**
     * 执行单条命令
     *
     * @param cmd
     * @param needRes
     * @return
     * @throws IOException
     */
    public static String execCommand(Boolean needRes, String cmd) {
        try {
            String res = execCmd(needRes, cmd);
            if (Boolean.TRUE.equals(needRes)) {
                LOGGER.info("executeCommand==>{}\n{}\n", cmd, res);
                return res;
            }
            return "";
        } catch (IOException e) {
            LOGGER.error("executeCommand==>{}\nWrong", cmd, e);
            return e.getMessage();
        }
    }

    private static String execCmd(Boolean needRes, String cmd) throws IOException {
        Process ps = Runtime.getRuntime().exec(new String[]{"/bin/bash", "-c", cmd});


        if (!Boolean.TRUE.equals(needRes)) {
            return "";
        }

        BufferedReader br = new BufferedReader(new InputStreamReader(ps.getInputStream()));
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = br.readLine()) != null) {
            sb.append(line).append("\n");
        }
        return sb.toString();
    }

}
