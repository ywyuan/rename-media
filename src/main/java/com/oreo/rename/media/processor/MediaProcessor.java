package com.oreo.rename.media.processor;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.oreo.rename.media.entity.domain.VideoMeta;
import com.oreo.rename.media.entity.enums.MediaTypeEnum;
import com.oreo.rename.media.extractor.Extractor;
import com.oreo.rename.media.extractor.ExtractorContext;
import com.oreo.rename.media.extractor.VideoExtractor;
import com.oreo.rename.media.util.FileLoader;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * MediaProcessor
 *
 * @author yuan
 * @since 2023/9/25 14:52
 */
@Slf4j
public class MediaProcessor {

    public static final ThreadLocal<List<String>> fallBackShotTimeFiles = ThreadLocal.withInitial(Lists::newArrayList);

    public static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd-HHmmss");
    /**
     * 文件路径
     */
    private String path;

    /**
     * 如果识别的拍摄日期小于该值，认为识别错误
     */
    private Integer wrongYearIfLess = 2000;

    private boolean recursively = false;
    private List<String> filterExtList = Collections.emptyList();

    public MediaProcessor setWrongYearIfLess(Integer wrongYearIfLess) {
        if (null != wrongYearIfLess && wrongYearIfLess > 0) {
            this.wrongYearIfLess = wrongYearIfLess;
        }
        return this;
    }

    public MediaProcessor setRecursively(boolean recursively) {
        this.recursively = recursively;
        return this;
    }

    public MediaProcessor setFilterExtList(List<String> filterExtList) {
        this.filterExtList = filterExtList;
        return this;
    }

    public String getPath() {
        return path;
    }

    public Integer getWrongYearIfLess() {
        return wrongYearIfLess;
    }

    /**
     * 支持识别的的文件
     */
    private final List<File> supportedFileList = Lists.newArrayList();
    private final Set<String> newUniqueNameSet = new HashSet<>();
    private Integer[] mediaHourCount = new Integer[24];

    public MediaProcessor(String path) {
        this.path = path;
    }

    /**
     * 视频meta 信息提取
     *
     * @return
     */
    public List<VideoMeta> getVideoMetaList() {
        List<File> files = FileLoader.load(path, recursively, filterExtList);
        List<VideoMeta> metaList = Lists.newArrayList();
        for (File file : files) {
            String name = file.getName();
            String fileExt = FileLoader.findFileExt(name);

            VideoExtractor videoExtractor = ExtractorContext.findVideoExtractor(fileExt);
            if (null == videoExtractor) {
                LOGGER.error("unsupported Movie name=" + name + " fileExt=" + fileExt);
                continue;
            }
            VideoMeta videoMeta = videoExtractor.extractMeta(file);
            if (null == videoMeta) {
                LOGGER.error("no videoMeta for name={} fileExt={}", name, fileExt);
                continue;
            }

            metaList.add(videoMeta);
        }

        return metaList;
    }

    public Map<String, List<String>> process() {
        return this.process(false);
    }

    /**
     * 处理媒体文件重命名
     *
     * @param doRename 是否执行重命名 false-只执行检查 true-检查+重命名
     * @return
     */
    public Map<String, List<String>> process(Boolean doRename) {
        Map<String, List<String>> supportExt = Maps.newLinkedHashMap();

        List<File> files = FileLoader.load(path, recursively, filterExtList);
        List<String> preCheckResult = this.preCheckSupport(files);
        supportExt.put("preCheck", preCheckResult);

        List<String> transList = Lists.newArrayList();

        int noShotTime = 0;
        List<String> noShotFileNames = Lists.newArrayList();

        int lessWrongShotTime = 0;
        List<String> lessWrongFileNames = Lists.newArrayList();

        // 前置 清理 ThreadLocal
        MediaProcessor.fallBackShotTimeFiles.remove();

        int renameOkCount = 0;
        int renameFailCount = 0;
        List<String> renameFailFiles = Lists.newArrayList();

        mediaHourCount = new Integer[24];
        // sort by name
        supportedFileList.sort(Comparator.comparing(File::getName));
        for (File file : supportedFileList) {
            String fileName = file.getName();
            // 提取拍摄时间
            LocalDateTime shotTime = this.extractMediaShotTime(file);
            if (null == shotTime) {
                LOGGER.warn("File={} noShotTime", fileName);
                noShotTime++;
                noShotFileNames.add(fileName);
                continue;
            }

            if (shotTime.getYear() < wrongYearIfLess) {
                lessWrongShotTime++;
                lessWrongFileNames.add(shotTime.format(formatter) + " ==> " + fileName);
                continue;
            }

            // 统计拍摄时间分布
            this.hourDistribute(shotTime);

            // 唯一文件名
            String newName = getUniqueFileName(shotTime, fileName, transList);
            LOGGER.trace("define newName={} for file={} ", newName, fileName);
            if (!Boolean.TRUE.equals(doRename)) {
                continue;
            }

            // 重命名
            boolean renameOk = this.renameMediaFile(file, newName);
            if (renameOk) {
                renameOkCount++;
            } else {
                renameFailCount++;
                renameFailFiles.add(fileName);
            }
        }

        Collections.sort(noShotFileNames);
        supportExt.put("noShotTime=" + noShotTime, noShotFileNames);
        supportExt.put("lessWrongShotTime=" + lessWrongShotTime, lessWrongFileNames);

        List<String> fallBackShotTimes = MediaProcessor.fallBackShotTimeFiles.get();
        supportExt.put("mindFallBackShotTime=" + fallBackShotTimes.size(), fallBackShotTimes);
        supportExt.put("mediaHourCount", this.formatHourDistribute(mediaHourCount));

        supportExt.put("transDup=" + transList.size(), transList);
        supportExt.put("renameOkCount=" + renameOkCount, Lists.newArrayList());
        supportExt.put("renameFail=" + renameFailCount, renameFailFiles);

        MediaProcessor.fallBackShotTimeFiles.remove();

        return supportExt;
    }

    private List<String> formatHourDistribute(Integer[] mediaHourCount) {
        List<String> list = Lists.newArrayList();
        int idx = 0;
        for (Integer integer : mediaHourCount) {
            Integer hourCount = Optional.ofNullable(integer).orElse(0);
            list.add(idx + "==>" + hourCount);

            idx++;
        }
        return list;
    }

    /**
     * 检查是否支持
     *
     * @param fileList
     * @return
     */
    private List<String> preCheckSupport(List<File> fileList) {
        supportedFileList.clear();
        Map<String, Integer> typeCountMap = Maps.newLinkedHashMap();

        for (File file : fileList) {
            String fileName = file.getName();
            String fileExt = FileLoader.findFileExt(fileName);

            if (MediaTypeEnum.support(fileExt)) {
                // support_
                incrementByKey("support_" + fileExt, typeCountMap);
                supportedFileList.add(file);
                continue;
            }

            // unsupported_
            incrementByKey("unsupported_" + fileExt, typeCountMap);
        }

        List<String> res = Lists.newArrayList();
        res.add("totalFile = " + fileList.size());
        for (Map.Entry<String, Integer> entry : typeCountMap.entrySet()) {
            String key = entry.getKey();
            Integer value = entry.getValue();

            res.add(key + " ==> " + value);
        }

        return res;
    }

    private void incrementByKey(String key, Map<String, Integer> typeCountMap) {
        Integer integer = typeCountMap.computeIfAbsent(key, k -> 0);
        typeCountMap.put(key, ++integer);
    }

    private LocalDateTime extractMediaShotTime(File file) {
        String fileName = file.getName();
        String ext = FileLoader.findFileExt(fileName);
        if (null == ext) {
            LOGGER.warn("File={} no ext", fileName);
            return null;
        }
        LocalDateTime shotTime = null;
        Extractor extractor = ExtractorContext.findExtractor(ext);
        if (null != extractor) {
            shotTime = extractor.extractShotTime(file);
        } else {
            LOGGER.warn("File={} no Extractor ext={}", fileName, ext);
        }

        return shotTime;
    }

    private void hourDistribute(LocalDateTime shotTime) {
        int hour = shotTime.getHour();
        Integer ct = mediaHourCount[hour];
        if (null == ct) {
            mediaHourCount[hour] = 0;
        }
        mediaHourCount[hour]++;
    }

    private String getUniqueFileName(LocalDateTime localDateTime, String fileName, List<String> transList) {

        String dotExt = "." + FileLoader.findFileExt(fileName);
        String formatName = localDateTime.format(formatter) + dotExt;

        LocalDateTime tmpDate = localDateTime;
        int i = 0;
        String tmpOld = formatName;
        while (newUniqueNameSet.contains(formatName)) {
            tmpDate = tmpDate.plusSeconds(1);
            i++;
            formatName = tmpDate.format(formatter) + dotExt;
        }

        if (i > 0) {
            // 命名冲突
            String trans = String.format("DupFile:%s trans from %s to %s ", fileName, tmpOld, formatName);
            transList.add(trans);
        }

        newUniqueNameSet.add(formatName);

        return formatName;
    }

    private boolean renameMediaFile(File file, String newName) {
        String absolutePath = file.getAbsolutePath();
        String oldName = file.getName();

        if (oldName.equals(newName)) {
            return true;
        }

        String newPath = absolutePath.replace(oldName, newName);
        File newFile = new File(newPath);
        try {
            FileUtils.moveFile(file, newFile);
            LOGGER.info("文件重命名成功！oldName={} newName={}", oldName, newName);
            return true;
        } catch (IOException e) {
            LOGGER.error("文件重命名失败：" + e.getMessage() + "oldName={} newName={}", oldName, newName);
            return false;
        }
    }
}
