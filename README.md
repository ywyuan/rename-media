# rename-media

## Description

个人多媒体文件重命名

- 读取照片、视频的拍摄时间，按照拍摄时间进行批量命名，方便查阅管理。

> 照片格式支持：JPEG、PNG、JPG、PNG
> 视频格式支持：MOV、MP4

- 命名冲突自动降级规避（同一时刻同类型多文件重名）
- 命名后的文件更加方便查阅和管理

## Getting started

- 下载启动

- 重命名：调用接口 /media/rename?path=xxx (path为本地多媒体文件所在文件夹)
- 预检查：调用接口 /media/check?path=xxx (path为本地多媒体文件所在文件夹)
- 支持格式：调用接口 /media/support

- 获取视频 meta 信息 127.0.0.1:8080/media/video/meta?path=/Users/yuan/Downloads/2023xxvideo
- 视频是否可压缩 127.0.0.1:8080/media/video/can/compress?path=/Users/yuan/Downloads/2023xxvideo

```shell
# 批量执行脚本命令
curl --location --request POST '127.0.0.1:8080/command/batch/execute' \
     --header 'Content-Type: application/json' \
     --data-raw '{
     "cmdList": [
     "pwd",
     "cd ~/Downloads/2023xxvideo",
     "ls -lSh |head -5 ",
     "pwd"
     ]
     }'
```

## Usage

- 这里是一个演示用例

```Java

@Slf4j
public class MediaProcessor {

    public static void main(String[] args) {
        String path = "/Users/yuan/Downloads/2023xxvideo";
        Object process = new MediaProcessor(path).process();
        LOGGER.warn(JSON.toJSONString(process));
    }
}
```

## Roadmap

- 后续将考虑支持更多文件类型

## Contributing

- 如果你有好的想法建议，欢迎共建

